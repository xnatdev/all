NRG Aggregator Library (nrg-all)
================================

The NRG aggregator library provides an easy way to build and deploy all of the
standard NRG framework libraries in a single operation. This aggregates all of
the upstream modules into a single build list.

Contents
--------

The referenced libraries are:

-   NRG parent (https://bitbucket.org/xnatdev/parent)

-   NRG Framework (https://bitbucket.org/xnatdev/framework)

-   NRG Transaction (https://bitbucket.org/xnatdev/transaction)

-   NRG Prefs (https://bitbucket.org/xnatdev/prefs)

-   NRG Configuration Service (https://bitbucket.org/xnatdev/config)

-   NRG Automation (https://bitbucket.org/xnatdev/automation)

-   NRG DICOM Tools (https://bitbucket.org/xnatdev/dicomtools)

-   NRG Anonymize (https://bitbucket.org/xnatdev/anonymize)

-   NRG Mail (https://bitbucket.org/xnatdev/mail)

-   NRG Notify (https://bitbucket.org/xnatdev/notify)

Building
--------

To build all of these modules, invoke Maven with the desired lifecycle phase.
For example, the following command will clean previous builds then build new 
jar files, along with archives containing each library's source code and JavaDocs,
run each library's unit tests, and install each jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~